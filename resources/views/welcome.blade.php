<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Laravel</title>

        <!-- Fonts -->
        <link href="{{asset('/css/app.css')}}" rel="stylesheet" /> 
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    </head>
    <body>
        <div class="container">     
        <div class="panel panel-primary">
          <div class="panel-heading">
            Upload Salary excel sheet
          </div>
          <div class="panel-body">

                @if (Session::has('message'))
                <?php
                    $message = Session::get('message')
                ?>
                    <div class="alert alert-{{$message['type']}}" role="alert">
                        {{$message['text']}}
                    </div>
                @endif

            



                
                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;" action="{{ route('upload-file') }}" class="form-horizontal" 
                method="post" enctype="multipart/form-data">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <input type="file" name="file" class="form-control"/>

                    {{ csrf_field() }}

                    <a href="{{asset('payment.csv')}}">Download Sample File with 10000 records</a>

                    <br/><br/>
                    <button class="btn btn-primary">
                    UPLOAD
                    </button>

                </form>
                <br/>

                </div>
                </div>
                </div>


                <div class="col-md-12 ">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Employees Details</h3>
                  </div>
                 
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        
                        <th>Sr No.</th>
                        <th>User Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Basic</th>
                        <th>Hra</th>
                        <th>DA</th>
                    
                    </tr> 
                  </thead>
                  <tbody>

                        @if($data->isNotEmpty())

                        <?php $i=1;?>
                        @foreach($data as $row)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->email}}</td>
                            <td>{{$row->basic}}</td>
                            <td>{{$row->hra}}</td>
                            <td>{{$row->da}}</td>
                            
                          </tr>

                          <?php $i++;?>
                        @endforeach

                        @else
                            <tr>
                            
                            <td colspan="10">SOrry!! No record Exist</td>
                            
                          </tr>
                        @endif
                        </tbody>
                </table>
            
              </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4">
                  </div>
                  <div class="col col-xs-8">
                    {{$data->links()}}
                  </div>
                </div>
              </div>
            </div>


                <script type="" src="{{asset('/js/app.js')}}"></script>
    </body>
</html>
