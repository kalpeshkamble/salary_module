<?php

use Illuminate\Database\Seeder;

class SalaryStructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('users')->orderBy('id')->chunk(100, function ($users) {
            $i=0;
            $salaryStructureData=array();
              foreach ($users as $user) {
                    
                    $salaryStructureData[$i]['user_id']=$user->id;
                    $salaryStructureData[$i]['basic']=rand(10000,50000);
                    $salaryStructureData[$i]['hra']=rand(1000,5000);
                    $salaryStructureData[$i]['da']=rand(1000,2000);

                    $i++;
              }

            \DB::table('salary_structure')->insert($salaryStructureData);
        });
    }
}
