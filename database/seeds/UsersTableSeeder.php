<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$usersData=array();
        $maxId = \DB::table('users')->max('id');
    	for($i=1;$i<=100;$i++)
    	{
            for($j=1;$j<=100;$j++)
            {
                $maxId+=1;
                $usersData[$j]['name']="User ". $maxId;
                $usersData[$j]['email']="user".$maxId.'@gmail.com';
                $usersData[$j]['password']=bcrypt('123456');
            }
    	   \DB::table('users')->insert($usersData);	
    	}
        
       
    }
}
