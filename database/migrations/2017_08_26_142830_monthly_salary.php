<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MonthlySalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('monthly_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->decimal('basic',10,2)->default(0);
            $table->decimal('hra',10,2)->default(0);
            $table->decimal('da',10,2)->default(0);

            $table->integer('pay_days')->dafault(0);
            
            $table->string('month',10);
            $table->integer('year');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
              //
        Schema::dropIfExists('monthly_salary');
    
    }
}
