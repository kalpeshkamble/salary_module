<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    //

	public function uploadFile(Request $request)
	{

		set_time_limit(0);

		
		$this->validate($request,
			array('file' =>'required')
			);

		$file=$request->file('file');
		//dd($file->getClientOriginalExtension());
		$ext=$file->getClientOriginalExtension();



		if($ext=='csv' || $ext=='xls' || $ext=='xlsx' ){
			
		}
		else
		{
			return back()->withInput()
			->withMessage(['type'=>'danger','text'=>'Only csv , xls or xlsx file allowed!']);
		}

		ini_set('auto_detect_line_endings', true);

		//$file=$request->file('file');

		$fileName=$file->getClientOriginalName();

		$file->move('payment/',$fileName);
		//dd($fileName);


	\Excel::selectSheetsByIndex(0)->load(public_path()."/payment/".$fileName, function($reader) {

    // Getting all results


    $results = $reader->get();

    $totalRows=count($results);
    $chunkSize=50;

    $no_of_chunk=ceil($totalRows/$chunkSize);


    $takeRows=$chunkSize;
    //$invalidUsers=array();

    for($i=1;$i<=$no_of_chunk;$i++)
    {
    	$skipRows=($i*$takeRows)-$takeRows;

    	$rows=$reader->skipRows($skipRows)->takeRows($takeRows)->get();


    	$monthSalaryData=array();

    	

 		foreach ($rows as $key => $value) {

 		$user_id=$value['userid'];
 		$pay_days=$value['paydays'];
 		$month=$value['month'];
 		$year=$value['year'];

 		$userData=\DB::table('users as u')
 		->join('salary_structure as ss','u.id','=','ss.user_id')
 		->where('u.id',$user_id)
 		->latest('ss.id')
 		->select('ss.*')
 		->first();

 		/*dd($userData);*/

 		if(!empty($userData->id))
 		{

 			$singleUserSalaryData=array();
			$month_number = date("n",strtotime($month));
			$no_of_days=cal_days_in_month(CAL_GREGORIAN, $month_number, $year);
 			$singleUserSalaryData['user_id']=$user_id;
 			$singleUserSalaryData['basic']=(($userData->basic*$pay_days)/$no_of_days);
 			$singleUserSalaryData['hra']=(($userData->hra*$pay_days)/$no_of_days);
 			$singleUserSalaryData['da']=(($userData->da*$pay_days)/$no_of_days);
 			$singleUserSalaryData['pay_days']=$pay_days;
 			$singleUserSalaryData['month']=$month;
 			$singleUserSalaryData['year']=$year;
			$monthSalaryData[]=$singleUserSalaryData;
 		}
 		/*else{

 				$invalidUsers[]=$value;
 		}*/

 	}

 	$flag=\DB::table('monthly_salary')->insert($monthSalaryData);

    }

   // echo"<hr><hr><hr>";

});

$data=array(
	'message'=>array(
		'type'=>'success',
		'text'=>'Payment processed  !!'
		)
	);

return redirect()->route('view-payment')->with($data);


	}




	public function viewPayment(Request $request)
	{

		$data=\DB::table('monthly_salary as ms')
		->join('users as u','ms.user_id','=','u.id')
		->select(
			'u.id',
			'u.name',
			'u.email',
			'ms.*'
			)
		->paginate(50);

		//dd($data);
		return view('view-payment',['data'=>$data]);
	}

	public function viewUsers(Request $request)
	{

		$data=\DB::table('users as u')
		->join('salary_structure as ss','ss.user_id','=','u.id')
		->select(
			'u.id',
			'u.name',
			'u.email',
			'ss.*'
			)
		->paginate(50);

		//dd($data);
		return view('view-payment',['data'=>$data]);
	}

}
