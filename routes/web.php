<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'web'], function() {
    // Place all your web routes here...
});


Route::get('/', function () {

$data=\DB::table('users as u')
		->join('salary_structure as ss','ss.user_id','=','u.id')
		->select(
			'u.id',
			'u.name',
			'u.email',
			'ss.*'
			)
		->paginate(50);

		// return $data;

    return view('welcome',['data'=>$data]);
});

Route::post('upload-file',['uses'=>'UsersController@uploadFile'])
->name('upload-file');



Route::get(
	'view-payment',
	['uses'=>'UsersController@viewPayment'])
->name('view-payment');